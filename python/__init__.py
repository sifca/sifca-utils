#!/usr/bin/env python
"""SIFCA analysis utilities
"""
# Used when "from sifca_utils import *"
__all__ = [ "plotting" ]
# Used when "import sifca_utils
import sifca_utils.plotting
