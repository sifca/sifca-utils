#!/usr/bin/env python
"""Utilities for data analysis to help in the daily work 
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

from six import string_types
import ROOT
from sifca_utils.plotting import set_sifca_style

class Bunch(object):
    """Utility class to load a ROOT file and access to its 
    contents in a pythonic way: trees and directories are 
    accessible directly as attributes, as they are showed
    when tabulating.

    Example
    -------
    After the initialization, the objects of the ROOT file
    can be accessed as data members of the instance

    >>> d = Bunch("rootfilename.root")
    """
    def __init__(self,fname,sifca_style=False):
        """Return a bunched ROOT file with all the contents
        of the ROOT file as data members of the instance

        Parameters
        ----------
        fname: str
            The root file
        sifca_style: bool
            Whether or not to set the SifcaStyle (see `plotting` module)

        """
        if sifca_style:
            st = set_sifca_style(stat_off=False)
            st.cd()
            ROOT.gROOT.ForceStyle()

        if isinstance(fname,string_types):
            self._f = ROOT.TFile.Open(fname)
        else: 
            self._f = fname
        for i in self._f.GetListOfKeys():
            self.__dict__[i.GetName()] = self._f.Get(i.GetName())
            if isinstance(self.__dict__[i.GetName()],ROOT.TDirectoryFile):
                self.__dict__[i.GetName()] = Bunch(self._f.Get(i.GetName()))

