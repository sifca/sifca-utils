#!/usr/bin/env python
"""Function distributions usually used in test beam analysis
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

import ROOT 

## -- FIXME wrapper to include the number of parameters for a function
#def npars(f,f,):


def peaks_searcher(h,xmin,xmax,sigma=1.0):
    """Search for peak of a distribution using the TSpectrum

    Parameters
    ----------
    h: ROOT.TH1
        The histogram
    xmin: float
        The minimum value the peak should be
    xmax: float
        The maximum value the peak should be
    sigma: float    
        The sigma of the searched peaks (see TSpectrum tutorial)

    Return
    ------
    peaks: list(float)
        Ordered (from min to max) list of found peaks
    """
    assert xmin < xmax,"xmin MUST be lower than xmax"
    # Use TSpectrum to find the available peaks
    batch=ROOT.gROOT.IsBatch()
    ROOT.gROOT.SetBatch(1)
    s=ROOT.TSpectrum()
    nfound = s.Search(h,sigma,'nobackground new nodraw goff')
    # Get the first peak comming from the left
    all_peaks = [ s.GetPositionX()[i] for i in range(nfound) ]
    ROOT.gROOT.SetBatch(batch)

    # And inside the range requested by the user
    peaks = list(filter(lambda x: xmin < x < xmax, all_peaks))
    if len(peaks) == 0:
        print("peaks_searcher WARNING: Not found any peak inside the"
                " window [{},{}]".format(xmin,xmax))

    return peaks

def landgaus(x,par):
    """Definition of a Landau convulated with gaus 

    Based on 
    https://root.cern.ch/root/html/tutorials/fit/langaus.C.html

    Note
    ----
    Implemented as c++ binding as speed is incremented 2 orders of
    magnitude otherwise

    Parameters
    ----------
    x: array
        The variable
    par: array
        par[0] = Most Probable (MP, location) parameter of Landau density
        par[1] = Width (scale) parameter of Landau density
        par[2] = Total area (integral -inf to inf, normalization constant)
        par[3] = Gaussian smearing
    """
    from math import pi,sqrt
    import ROOT 

    if not hasattr(landgaus,'_cmpcode'):
        cppcode="""
        #include <cmath>
        #include <algorithm>

        double landgaus(double *x, double *par)
        {
            // Landau maximum location: in the Landau distribution
            // (represented by the CERNLIB approximation), the maximum
            // is located at -0.22278298 with the location parameter = 0
            // This shift is corrected within this function, so that the actual 
            // maximum is identical to the MP parameter
            const double mpshift  = -0.22278298;
        """
        cppcode +="""
            const double pi = {};
        """.format(pi)
        
        cppcode += """
            // MP shift correction
            const double mpc = par[0]-mpshift*par[1];
            // -- Fit parameters:
            //   par[0] = Most Probable (MP, location) parameter of Landau density
            //   par[1] = Width (scale) parameter of Landau density
            //   par[2] = Total area (integral -inf to inf, normalization constant)
            //   par[3] = Gaussian smearing
            
            // -- Control constants
            //  Number of convolution steps
            const double np = 100.0;
            //  Convolution extends to +- sc gaussian sigmas
            const double sc = 5.0;
            // -- Range of convolution integral
            const double xlow = std::max(0.0,x[0]-sc*par[3]);
            const double xupp = x[0]+sc*par[3];
            const double step =(xupp-xlow)/np;
            
            //-- Convolution integral of Landau and Gaussian by sum
            double total = 0.0;
            for(int i=1; i <int(np/2.0)+1; ++i)
            {
                const double xi = xlow+(i-0.5)*step;
                const double xxi= xupp-(i-0.5)*step;

                total += TMath::Landau(xi,mpc,par[1])/par[1]*TMath::Gaus(x[0],xi,par[3])+ 
                           TMath::Landau(xxi,mpc,par[1])/par[1]*TMath::Gaus(x[0],xxi,par[3]);
            }
                        
            return par[2]*1./std::sqrt(pi)*step*total/par[3];
        }
        """
        _= ROOT.gInterpreter.ProcessLine(cppcode)
        landgaus._cmpcode = ROOT.landgaus
        landgaus.npars = 4
    return landgaus._cmpcode(x,par)

def erlang_landgaus(x,par):
    """Definition of a Poisson + Landau(x)Gaus 

    Based on 
    https://root.cern.ch/root/html/tutorials/fit/langaus.C.html

    Note
    ----
    Implemented as c++ binding as speed is incremented 2 orders of
    magnitude otherwise

    Parameters
    ----------
    x: array
        The variable
    par: array
        Landgaus Parameters
        ===================
        par[0] = Most Probable (MP, location) parameter of Landau density
        par[1] = Width (scale) parameter of Landau density
        par[3] = Gaussian smearing
        
        Erlang Parameters
        ==================
        par[4] = Shape of the distribution (==1 -> exponential, larger values -> gaussian)
        par[5] = Location --> fixed to zero (Erlang) XXX TO BE CHANGED
        par[6] = Scale (i.e. reciprocal of the rate, in our case, the average number of 
                 charge measured each time, due to this component, i.e. created charge in the
                 non-depleted zone)
        
        Common
        ======
        par[2] = Total area (integral -inf to inf, normalization constant)
        par[7] = the fraction of events corresponding to the Landgaus

    Notes
    -----
    TBD: Fix parameters to sensitive values (0<par[4] < par[0], par[5]-fixed 0, par[6] > 0, ...)

    """
    from math import pi,sqrt
    import ROOT 

    if not hasattr(erlang_landgaus,'_cmpcode'):
        cppcode="""
        #include <cmath>
        #include <algorithm>

        double erlang_landgaus(double *x, double *par)
        {
            // Landau maximum location: in the Landau distribution
            // (represented by the CERNLIB approximation), the maximum
            // is located at -0.22278298 with the location parameter = 0
            // This shift is corrected within this function, so that the actual 
            // maximum is identical to the MP parameter
            const double mpshift  = -0.22278298;
        """
        cppcode +="""
            const double pi = {};
        """.format(pi)
        
        cppcode += """
            // MP shift correction
            const double mpc = par[0]-mpshift*par[1];
            // -- Fit parameters:
            //   par[0] = Most Probable (MP, location) parameter of Landau density
            //   par[1] = Width (scale) parameter of Landau density
            //   par[2] = Total area (integral -inf to inf, normalization constant)
            //   par[3] = Gaussian smearing
            
            // -- Control constants
            //  Number of convolution steps
            const double np = 100.0;
            //  Convolution extends to +- sc gaussian sigmas
            const double sc = 5.0;
            // -- Range of convolution integral
            const double xlow = std::max(0.0,x[0]-sc*par[3]);
            const double xupp = x[0]+sc*par[3];
            const double step =(xupp-xlow)/np;
            
            //-- Convolution integral of Landau and Gaussian by sum
            double total = 0.0;
            for(int i=1; i <int(np/2.0)+1; ++i)
            {
                const double xi = xlow+(i-0.5)*step;
                const double xxi= xupp-(i-0.5)*step;

                total += TMath::Landau(xi,mpc,par[1])/par[1]*TMath::Gaus(x[0],xi,par[3])+ 
                           TMath::Landau(xxi,mpc,par[1])/par[1]*TMath::Gaus(x[0],xxi,par[3]);
            }

            const float norm = par[2];
                        
            return norm*(par[7]*(1./std::sqrt(pi)*step*total/par[3])+
                +(1.0-par[7])*TMath::GammaDist(x[0],par[4],par[5],par[6]));
        }
        """
        _= ROOT.gInterpreter.ProcessLine(cppcode)
        erlang_landgaus._cmpcode = ROOT.erlang_landgaus
        erlang_landgaus.npars = 8
    return erlang_landgaus._cmpcode(x,par)


def erlang_conv_landgaus(x,par):
    """Definition of a Poisson + Landau(x)Gaus 

    Based on 
    https://root.cern.ch/root/html/tutorials/fit/langaus.C.html

    Note
    ----
    Implemented as c++ binding as speed is incremented 2 orders of
    magnitude otherwise

    Parameters
    ----------
    x: array
        The variable
    par: array
        Landgaus Parameters
        ===================
        par[0] = Most Probable (MP, location) parameter of Landau density
        par[1] = Width (scale) parameter of Landau density
        par[3] = Gaussian smearing
        
        Erlang Parameters
        ==================
        par[4] = Shape of the distribution (==1 -> exponential, larger values -> gaussian)
        par[5] = Location --> fixed to zero (Erlang) XXX TO BE CHANGED
        par[6] = Scale (i.e. reciprocal of the rate, in our case, the average number of 
                 charge measured each time, due to this component, i.e. created charge in the
                 non-depleted zone)
        
        Common
        ======
        par[2] = Total area (integral -inf to inf, normalization constant)
        par[7] = the fraction of events corresponding to the Landgaus

    Notes
    -----
    TBD: Fix parameters to sensitive values (0<par[4] < par[0], par[5]-fixed 0, par[6] > 0, ...)

    """
    from math import pi,sqrt
    import ROOT 

    if not hasattr(erlang_conv_landgaus,'_cmpcode'):
        cppcode="""
        #include <cmath>
        #include <algorithm>

        double erlang_conv_landgaus(double *x, double *par)
        {
            // Landau maximum location: in the Landau distribution
            // (represented by the CERNLIB approximation), the maximum
            // is located at -0.22278298 with the location parameter = 0
            // This shift is corrected within this function, so that the actual 
            // maximum is identical to the MP parameter
            const double mpshift  = -0.22278298;
        """
        cppcode +="""
            const double pi = {};
        """.format(pi)
        
        cppcode += """
            // MP shift correction
            const double mpc = par[0]-mpshift*par[1];
            // -- Fit parameters:
            //   par[0] = Most Probable (MP, location) parameter of Landau density
            //   par[1] = Width (scale) parameter of Landau density
            //   par[2] = Total area (integral -inf to inf, normalization constant)
            //   par[3] = Gaussian smearing
            
            // -- Control constants
            //  Number of convolution steps
            const double np = 100.0;
            //  Convolution extends to +- sc gaussian sigmas
            const double sc = 5.0;
            // -- Range of convolution integral
            const double xlow = std::max(0.0,x[0]-sc*par[3]);
            const double xupp = x[0]+sc*par[3];
            const double step =(xupp-xlow)/np;
            
            //-- Convolution integral of Landau and Gaussian by sum
            double total = 0.0;
            for(int i=1; i <int(np/2.0)+1; ++i)
            {
                const double xi = xlow+(i-0.5)*step;
                const double xxi= xupp-(i-0.5)*step;

                total += TMath::Landau(xi,mpc,par[1])/par[1]*TMath::Gaus(x[0],xi,par[3])+ 
                           TMath::Landau(xxi,mpc,par[1])/par[1]*TMath::Gaus(x[0],xxi,par[3]);
            }

            const float norm = par[2];
                        
            return (1./std::sqrt(pi)*step*total/par[3])*((1.0-norm)*TMath::GammaDist(x[0],par[4],par[5],par[6]) + norm);
        }
        """
        _= ROOT.gInterpreter.ProcessLine(cppcode)
        erlang_conv_landgaus._cmpcode = ROOT.erlang_conv_landgaus
        erlang_conv_landgaus.npars = 7
    return erlang_conv_landgaus._cmpcode(x,par)



def gaus_landgaus(x,par):
    """Definition of a Gaus + Landau(x)Gaus 
    where the Gaus is related to a low edge contribution

    Based on 
    https://root.cern.ch/root/html/tutorials/fit/langaus.C.html

    Note
    ----
    Implemented as c++ binding as speed is incremented 2 orders of
    magnitude otherwise

    Parameters
    ----------
    x: array
        The variable
    par: array
        Landgaus Parameters
        ===================
        par[0] = Most Probable (MP, location) parameter of Landau density
        par[1] = Width (scale) parameter of Landau density
        par[3] = Gaussian smearing
        
        Gaus Parameters
        ==================
        par[4] = Mean 
        par[5] = Sigma
        
        Common
        ======
        par[2] = Total area (integral -inf to inf, normalization constant)
        par[6] = the fraction of events corresponding to the Landgaus

    Notes
    -----
    TBD: Fix parameters to sensitive values (0<par[4] < par[0], par[5]-fixed 0, par[6] > 0, ...)

    """
    from math import pi,sqrt
    import ROOT 

    if not hasattr(gaus_landgaus,'_cmpcode'):
        cppcode="""
        #include <cmath>
        #include <algorithm>

        double gaus_landgaus(double *x, double *par)
        {
            // Landau maximum location: in the Landau distribution
            // (represented by the CERNLIB approximation), the maximum
            // is located at -0.22278298 with the location parameter = 0
            // This shift is corrected within this function, so that the actual 
            // maximum is identical to the MP parameter
            const double mpshift  = -0.22278298;
        """
        cppcode +="""
            const double pi = {};
        """.format(pi)
        
        cppcode += """
            // MP shift correction
            const double mpc = par[0]-mpshift*par[1];
            // -- Fit parameters:
            //   par[0] = Most Probable (MP, location) parameter of Landau density
            //   par[1] = Width (scale) parameter of Landau density
            //   par[2] = Total area (integral -inf to inf, normalization constant)
            //   par[3] = Gaussian smearing
            
            // -- Control constants
            //  Number of convolution steps
            const double np = 100.0;
            //  Convolution extends to +- sc gaussian sigmas
            const double sc = 5.0;
            // -- Range of convolution integral
            const double xlow = std::max(0.0,x[0]-sc*par[3]);
            const double xupp = x[0]+sc*par[3];
            const double step =(xupp-xlow)/np;
            
            //-- Convolution integral of Landau and Gaussian by sum
            double total = 0.0;
            for(int i=1; i <int(np/2.0)+1; ++i)
            {
                const double xi = xlow+(i-0.5)*step;
                const double xxi= xupp-(i-0.5)*step;

                total += TMath::Landau(xi,mpc,par[1])/par[1]*TMath::Gaus(x[0],xi,par[3])+ 
                           TMath::Landau(xxi,mpc,par[1])/par[1]*TMath::Gaus(x[0],xxi,par[3]);
            }

            const float norm = par[2];
                        
            return norm*(par[6]*(1./std::sqrt(pi)*step*total/par[3])+
                +(1.0-par[6])*TMath::Gaus(x[0],par[4],par[5],true));
        }
        """
        _= ROOT.gInterpreter.ProcessLine(cppcode)
        gaus_landgaus._cmpcode = ROOT.gaus_landgaus
        gaus_landgaus.npars = 7
    return gaus_landgaus._cmpcode(x,par)

def gaus_erlang_landgaus(x,par):
    """Definition of a Gaus + Landau(x)Gaus 
    where the Gaus is related to a low edge contribution

    Based on 
    https://root.cern.ch/root/html/tutorials/fit/langaus.C.html

    Note
    ----
    Implemented as c++ binding as speed is incremented 2 orders of
    magnitude otherwise

    Parameters
    ----------
    x: array
        The variable
    par: array
        Landgaus Parameters
        ===================
        par[0] = Most Probable (MP, location) parameter of Landau density
        par[1] = Width (scale) parameter of Landau density
        par[3] = Gaussian smearing
        
        Erlang Parameters
        ==================
        par[4] = Shape of the distribution (==1 -> exponential, larger values -> gaussian)
        par[5] = Location --> fixed to zero (Erlang) XXX TO BE CHANGED
        par[6] = Scale (i.e. reciprocal of the rate, in our case, the average number of 
                 charge measured each time, due to this component, i.e. created charge in the
                 non-depleted zone)
        
        Gaus Parameters
        ==================
        par[7] = Mean 
        par[8] = Sigma
        
        Common
        ======
        par[2] = Total area (integral -inf to inf, normalization constant)
        par[9] = the fraction of events corresponding to the Landgaus (w.r.t. the Erlang)
        par[10] = the fraction of events corresponding to the Erlang_Landgaus (w.r.t. the low edge Gaus)

    Notes
    -----
    TBD: Fix parameters to sensitive values (0<par[4] < par[0], par[5]-fixed 0, par[6] > 0, ...)

    """
    from math import pi,sqrt
    import ROOT 

    if not hasattr(gaus_erlang_landgaus,'_cmpcode'):
        cppcode="""
        #include <cmath>
        #include <algorithm>

        double gaus_erlang_landgaus(double *x, double *par)
        {
            // Landau maximum location: in the Landau distribution
            // (represented by the CERNLIB approximation), the maximum
            // is located at -0.22278298 with the location parameter = 0
            // This shift is corrected within this function, so that the actual 
            // maximum is identical to the MP parameter
            const double mpshift  = -0.22278298;
        """
        cppcode +="""
            const double pi = {};
        """.format(pi)
        
        cppcode += """
            // MP shift correction
            const double mpc = par[0]-mpshift*par[1];
            // -- Fit parameters:
            //   par[0] = Most Probable (MP, location) parameter of Landau density
            //   par[1] = Width (scale) parameter of Landau density
            //   par[2] = Total area (integral -inf to inf, normalization constant)
            //   par[3] = Gaussian smearing
            
            // -- Control constants
            //  Number of convolution steps
            const double np = 100.0;
            //  Convolution extends to +- sc gaussian sigmas
            const double sc = 5.0;
            // -- Range of convolution integral
            const double xlow = std::max(0.0,x[0]-sc*par[3]);
            const double xupp = x[0]+sc*par[3];
            const double step =(xupp-xlow)/np;
            
            //-- Convolution integral of Landau and Gaussian by sum
            double total = 0.0;
            for(int i=1; i <int(np/2.0)+1; ++i)
            {
                const double xi = xlow+(i-0.5)*step;
                const double xxi= xupp-(i-0.5)*step;

                total += TMath::Landau(xi,mpc,par[1])/par[1]*TMath::Gaus(x[0],xi,par[3])+ 
                           TMath::Landau(xxi,mpc,par[1])/par[1]*TMath::Gaus(x[0],xxi,par[3]);
            }

            const float norm = par[2];
                        
            return norm*(par[10]*(par[9]*(1./std::sqrt(pi)*step*total/par[3])
                      +(1.0-par[9])*TMath::GammaDist(x[0],par[4],par[5],par[6]))
                   +(1.0-par[10])*TMath::Gaus(x[0],par[7],par[8],true));
        }
        """
        _= ROOT.gInterpreter.ProcessLine(cppcode)
        gaus_erlang_landgaus._cmpcode = ROOT.gaus_erlang_landgaus
        gaus_erlang_landgaus.npars = 11
    return gaus_erlang_landgaus._cmpcode(x,par)

